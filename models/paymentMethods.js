const allPaymentMethods = [{
    methodId: 1,
    methodName: "cash",
    status: true
},
{
    methodId: 2,
    methodName: "debit",
    status: true
},
{
    methodId: 3,
    methodName: "credit",
    status: true
}
]


module.exports = allPaymentMethods