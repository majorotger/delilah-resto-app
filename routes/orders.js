const express = require('express');
var router = express.Router();
const moment = require("moment");

const allProducts = require('../models/products')
const allMiddlewares = require('../middlewares/allMiddlewares.js')
const allUsers = require('../models/users')
const allOrders = require('../models/orders')
const allPaymentMethods = require('../models/paymentMethods')
const allStatus = ["pending", "confirmed", "preparing", "sending", "cancelled","delivered"]


// Los usuarios registrados puedan realizar pedidos de los productos que desean consumir
router.post('/', allMiddlewares.checkUserLog, allMiddlewares.checkOrderData, function (req, res) {
    const user = allUsers.find(user => user.userId === parseInt(req.headers.userid))
    let totalAcount = 0
    const ID = allOrders.length + 1    
    const productList = req.body.prodIdList
    productList.forEach(productList => {
        let product = allProducts.find(allProducts => allProducts.productId === parseInt(productList.prodId))
        if(product != undefined){
            if(!product.active) return res.status(401).json({"message":'The product is not able'})
            totalAcount = totalAcount + (product.price*productList.qty)
        }
    })
    if(totalAcount === 0) return res.status(404).json({"message":'Error'})
    order = req.body
    order.orderId = ID
    let payMethod = allPaymentMethods.find(allPaymentMethods => allPaymentMethods.methodId === parseInt(order.methodId))
    if(!payMethod) return res.status(422).json({"message":'The payment method is not valid'})
    const index = allPaymentMethods.indexOf(payMethod)
    if(!allPaymentMethods[index].status) return res.status(401).json({"message":'The payment method is not able'})
    if (!order.address) order.address = user.address
    order.status = allStatus[0]    
    order.userId = user.userId
    order.totalAcount = totalAcount
    allOrders.push(order)
    res.status(201).json({"message":'Order created successfully'})
})

//CARRITO CHECKOUT para poner el estado en "confirmed" una vez terminada la carga de la orden
router.put('/checkout/:orderId', allMiddlewares.checkUserLog, allMiddlewares.checkIdOrder, function (req, res){
    const order = allOrders.find(allOrders => allOrders.orderId === parseInt(req.params.orderId))
    if(!order) return res.status(404).json({"message":'Order does not exist'})
    order.status = "confirmed"
    const theDate = new Date()
    order.hour = moment(theDate).format('HH:mm:ss')
    res.status(200).json({"message":'The order was confirmed successfully'})
})

//Los usuarios registrados puedan ver el historial de sus pedidos y Los administradores puedan ver todos los pedidos
router.get('/', allMiddlewares.checkUserLog, function (req, res) {
    const user = allUsers.find(allUsers => allUsers.userId === parseInt(req.headers.userid))  
    if(user.isAdmin) return res.status(200).json({'Orders': allOrders})
    const ordersHistory = allOrders.filter(allOrders => allOrders.userId === parseInt(user.userId))
    return res.status(200).json({ "ordersHistory": ordersHistory })
})

//Los usuarios registrados puedan modificar un pedido todavia abierto
router.put('/:orderId', allMiddlewares.checkUserLog, allMiddlewares.checkOrderData, function (req, res) {
    const user = allUsers.find(allUsers => allUsers.userId === parseInt(req.headers.userid))
    let totalAcount = 0
    const order = allOrders.find(allOrders => allOrders.orderId === parseInt(req.params.orderId))
    if (!order) res.status(404).json({"message":'Order does not exist'})
    if(!(user.userId === order.userId)) res.status(401).json({"message":'Unauthorized'})
    if(!(order.status === "pending")) res.status(401).json({"message":'The order is not able to be modified'})
    let payMethod = allPaymentMethods.find(allPaymentMethods => allPaymentMethods.methodId === parseInt(order.methodId))
    if(!payMethod) res.status(422).send({"message":'The payment method is not valid'})
    const index = allPaymentMethods.indexOf(payMethod)
    if(!allPaymentMethods[index].status) return res.status(401).json({"message":'The payment method is not able'})
    order.methodId = req.body.methodId         
    order.prodIdList = req.body.prodIdList
    if(!req.body.address) order.address = user.address
    order.address = req.body.address
    const productList = order.prodIdList
    productList.forEach(productList => {
        let product = allProducts.find(allProducts => allProducts.productId === parseInt(productList.prodId))
        if(product) totalAcount = totalAcount + (product.price*productList.qty)
    })
    order.userId = user.userId
    order.totalAcount = totalAcount
    res.status(200).json({"message":'Order updated successfully'})
})

//Los administradores pueden cambiar el estado de los pedidos (pending/confirmed/preparing/sending/cancelled/delivered)
router.put('/changeStatus/:orderId', allMiddlewares.checkUserLog, allMiddlewares.checkIdOrder, allMiddlewares.checkAdmin, function (req, res) {
    const order = allOrders.find(allOrders => allOrders.orderId === parseInt(req.params.orderId))
    if(!order) return res.status(404).json({"message":'Order does not exist'})
    if(order.status === "pending") return res.status(401).json({"message":'The customer has not yet confirmed the order'})
    const status = allStatus.find(allStatus => allStatus === req.body.status)
    if(!status) return res.status(422).json({"message":'The status is not valid'})
    order.status = status
    return res.status(200).json({"message":'Status updated successfully'})
})

module.exports = router