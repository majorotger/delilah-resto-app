const express = require('express')
const router = express.Router()

const allMiddlewares = require('../middlewares/allMiddlewares')
const allPaymentMethods = require('../models/paymentMethods')

//los administradores puedan listar medios de pago
router.get('/', allMiddlewares.checkUserLog, function (req, res) {
    res.status(200).json({"paymentMethods": allPaymentMethods})
})

//los administradores puedan crear nuevos medios de pago
router.post('/', allMiddlewares.checkUserLog, allMiddlewares.checkAdmin, function (req, res) {
    const ID = allPaymentMethods.length + 1
    allPaymentMethods.push(req.body)
    allPaymentMethods[allPaymentMethods.length-1].methodId = ID
    if(!allPaymentMethods[allPaymentMethods.length-1].status) allPaymentMethods[allPaymentMethods.length-1].status = true
    res.status(201).json({"message": 'Payment method created successfully'})
})

// los administradores puedan editar medios de pago
router.put('/:methodId', allMiddlewares.checkUserLog, allMiddlewares.checkAdmin, function (req, res) {
    const payMethod = allPaymentMethods.find(allPaymentMethods => allPaymentMethods.methodId === parseInt(req.params.methodId))
    if(!payMethod) return res.status(404).json({"message":'The payment method does not exist'})
    const index = allPaymentMethods.indexOf(payMethod)
    allPaymentMethods.splice(index, 1, req.body)
    allPaymentMethods[index].methodId = parseInt(req.params.methodId)
    allPaymentMethods[index].status = true
    res.status(200).json({"message": 'Payment method updated successfully'})
})

// los administradores puedan borrar medios de pago
router.delete('/:methodId', allMiddlewares.checkUserLog, allMiddlewares.checkAdmin, function (req, res) {
    const payMethod = allPaymentMethods.find(allPaymentMethods => allPaymentMethods.methodId === parseInt(req.params.methodId))   
    if(!payMethod) return res.status(404).json({"message":'The payment method does not exist'})
    const index = allPaymentMethods.indexOf(payMethod)
    allPaymentMethods[index].status = false
    res.status(200).json({"message": 'Payment method deleted successfully'})
})

module.exports = router