const allUsers = require('../models/users')

//Validacion de datos obligatorios
function checkUserData (req, res, next) {
    if (!req.body.userName) return res.status(422).json({"message": "The nick name is required"})
    if (!req.body.name) return res.status(422).json({"message": "The name is required"})
    if (!req.body.email) return res.status(422).json({"message": "The email is required"})
    if (!req.body.phone) return res.status(422).json({"message": "The phone number is required"})
    if (!req.body.address) return res.status(422).json({"message": "The address is required"})
    if (!req.body.password) return res.status(422).json({"message": "The password is required"})
    next ()
}

//Validacion de datos obligatorios
function checkProductData (req, res, next){
    if (!req.body.productName) return res.status(422).json({"message": "The product name is required"})
    if (!req.body.price) return res.status(422).json({"message": "The price is required"})
    if (isNaN(req.body.price)) return res.status(400).json({"message": "The price must be a number"})
    next ()
}

//Validacion de datos obligatorios
function checkOrderData (req, res, next){
    if (req.body.productList) return res.status(422).json({"message": "The product list is required"})
    if (!req.body.methodId) return res.status(422).json({"message": "The payment method is required"})
    next ()
}

//Validacion si el usuario esta logeado
function checkUserLog(req, res, next) {
    if (!Number.isInteger(parseInt(req.headers.userid))) res.status(400).json({"message": "The id must be an integer"})
    const user = allUsers.find(allUsers => allUsers.userId === parseInt(req.headers.userid))
    if (!user) res.status(401).json({"message": `You are not logged in`})
    next ()
}

//Validacion si el id es un entero
function checkIdProduct(req, res, next) {
    const idProd = parseInt(req.params.productId);
    if (!Number.isInteger(idProd)) return res.status(422).json({msg: "The id must be an integer"})
    next()
}

//Validacion si el id es un entero
function checkIdOrder(req, res, next) {
    const idOrder = parseInt(req.params.orderId)
    if (!Number.isInteger(idOrder)) res.status(422).json({"message": "The id must be an integer"})
    next()
}

//Validacion de si el usuario es administrador
function checkAdmin (req, res, next){
    const user = allUsers.find(allUsers => allUsers.userId === parseInt(req.headers.userid))
    if(!user.isAdmin) return res.status(401).json({"message": "You are not able to perform this action"})
    next ()
}

//Que no hayan emails duplicados en el registro
function checkDuplicateEmail(req, res, next){
    const emailUsuario = allUsers.find(allUsers => allUsers.email === req.body.email)
    if(emailUsuario) return res.status(401).json({"message": "email already exists"})
    next()
}

module.exports = { checkUserData, checkProductData, checkOrderData, checkUserLog, checkIdProduct, checkIdOrder, checkAdmin, checkDuplicateEmail }