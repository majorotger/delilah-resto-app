const express = require('express')
var router = express.Router()

const allUsers = require('../models/users')
const allMiddlewares = require('../middlewares/allMiddlewares')


// Los usuarios puedan crear una cuenta en la aplicación, para la creacion de ID se toma en cuenta que un 
//usuario nunca sera eliminado
router.post('/register', allMiddlewares.checkUserData, allMiddlewares.checkDuplicateEmail ,function (req, res) {
    const ID = allUsers.length + 1
    allUsers.push(req.body)
    if (allUsers.length-1 != 0){
        allUsers[allUsers.length-1].isAdmin = false
    }
    allUsers[allUsers.length-1].userId = ID
    res.status(201).json({"message": 'User created successfully'})
})

//Solo el administrador puede ver el listado de usuarios registrados
router.get('/', allMiddlewares.checkAdmin, function (req, res) {
    res.status(200).json({"Users":allUsers})
})

//Los usuarios puedan hacer login con su usuario y contraseña del registro
router.post('/login', function (req, res) {
    const user = allUsers.find(allUsers => allUsers.email === req.body.email && allUsers.password === req.body.password)
    if(!user) res.status(401).json({"message": 'Invalid user'})
    res.status(200).json({"message": 'Session successfully started. The userId is: ' + user.userId})
})


module.exports = router