const express = require('express')
const router = express.Router()

const allProducts = require('../models/products')
const allMiddlewares = require('../middlewares/allMiddlewares.js')
const allUsers = require('../models/users')


//Un usuario debe estar logueado para ver todos los productos disponibles para realizar una compra
router.get('/', allMiddlewares.checkUserLog, function (req, res) {
    res.status(200).json({"Products": allProducts})
})

//Los usuarios administradores puedan dar de alta nuevos productos
router.post('/', allMiddlewares.checkProductData, allMiddlewares.checkAdmin, function (req, res) {
    const ID = allProducts.length + 1    
    allProducts.push(req.body)
    allProducts[allProducts.length-1].productId = ID
    if(!allProducts[allProducts.length-1].active) allProducts[allProducts.length-1].active = true
    res.status(201).json({"message":'Product created successfully'})
})
 

//Los usuarios administradores puedan editar un producto
router.put('/:productId', allMiddlewares.checkUserLog, allMiddlewares.checkAdmin, allMiddlewares.checkIdProduct, allMiddlewares.checkProductData, function (req, res) {
    const product = allProducts.find(allProducts => allProducts.productId === parseInt(req.params.productId))
    if (!product) return res.status(404).json({"message":'Product does not exist'})
    const index = allProducts.indexOf(product)
    allProducts.splice(index, 1, req.body)
    allProducts[index].productId = parseInt(req.params.productId)
    allProducts[index].active = true
    res.status(200).json({"message": 'Product updated successfully'})   
})

//Los usuarios administradores puedan eliminar un producto
router.delete('/:productId', allMiddlewares.checkUserLog, allMiddlewares.checkIdProduct, allMiddlewares.checkAdmin, function (req, res)  {
    const product = allProducts.find(allProducts => allProducts.productId === parseInt(req.params.productId))
    if (!product) return res.status(404).json({"message":'Product does not exist'})
    const index = allProducts.indexOf(product)
    allProducts[index].active = false
    res.status(200).json({"message":'Product deleted successfully'})
})


module.exports = router