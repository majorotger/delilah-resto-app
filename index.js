const config = require('./config')
const express = require('express')
const app = express()

const cors = require('cors')

app.use(cors());
app.use(express.json())



const users = require('./routes/users')
app.use('/users', users)

const products = require('./routes/products')
app.use('/products', products)

const orders = require('./routes/orders')
app.use('/orders', orders)

const paymentMethods = require('./routes/paymentMethods')
app.use('/paymentMethods', paymentMethods)


app.listen(config.port, function () {
    console.log(`Escuchando el puerto ${config.port}`);
})