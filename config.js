require("dotenv").config()

const config = {
    port: process.env.NODE_PORT || 5000,
}

module.exports = config