# Delilah Resto App

API para pedidos y delivery de comidas

Proyecto curso Desarrollo Web Backend de Acámica

Para poder testear la API se requiere
1. node v14.17.3
2. npm v6.14.13
3. dependencias express, moment, dotenv, swagger


Iniciando
1. clonar el repositorio desde la URL https://gitlab.com/majorotger/delilah-resto-app.git
2. instalar las dependencias mencionadas anteriormente
3. ejecutar el comando node index.js
4. para acceder a la documentacion se debe utilizar el archivo app_doc.yaml del repositorio, alli se detallaran los endpoints, métodos disponibles y la información necesaria para hacer uso de los mismos



